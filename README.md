### Robo shop

* Robo shop is a open source containerized microservices application. It has few Data storage containers like MongoDB, Redis, RabbitMQ, MySQL.
* Api layer calls these Data containers to store the data
* UI layer calls the API layers for user clicks.

So the dependency is
* Data layer
* API layer
* UI layer

![alt text](robo-shop.jpg)



